/// <reference types="Cypress" />

describe('The Profile Page', function () {
    it('profile integration', function () {
        //Integration - profile in navbar should not be visible before login
        cy.get('#nav-profile').should('have.class', 'hide')

        //Integration - login should navigate to workout.html
        this.login(this.user)
        cy.url().should('include', 'workouts.html')

        //Integration - profile in navbar should be visible after login
        cy.get('#nav-profile').should('not.have.class', 'hide').click()

        //Integration - click on profile should open profile page
        cy.url().should('include', 'profile.html')
        cy.wait(3000);

        //Integration - check if displayed information is correct
        cy.get('input[name="username"]').should('have.value', this.user.username)

        //Integration - edit button should be visible
        cy.get('#btn-edit-profile').should('not.have.class', 'hide').click();
        cy.get('#btn-edit-profile').should('have.class', 'hide')
    })

    it('system - edit userinformation', function () {
        this.login(this.user)
        cy.visit('/profile.html')
        cy.wait(3000)

        //System - Edit phonenumber with random number
        cy.get('#btn-edit-profile').click();
        let number = Math.floor(Math.random() * 111111)
        cy.get('input[name="phone_number"]').type(number)

        //System - Save
        cy.get('#btn-save-edit-profile').should('not.have.class', 'hide').click()

        //System - Displays success alert
        cy.contains('User information sucessfully updated').parent().should('have.class', 'alert-success')

        //System - Input should be updated
        cy.get('input[name="phone_number"]').should('have.value', number)
    })

    it('system - edit userinformation (invalid information)', function () {
        this.login(this.user)
        cy.visit('/profile.html')
        cy.wait(3000)

        //System - should not accept invalid email
        cy.get('#btn-edit-profile').click();
        cy.get('input[name="email"]').type('a') //try 'a' as email
        cy.get('#btn-save-edit-profile').click() //click to save
        cy.contains('Could not update user profile').parent().should('have.class', 'alert-warning') //should display alert warning
        cy.contains('Could not update user profile').parent().should('contain', 'Enter a valid email address.') //warning should contain correct information
    })

    it('system - change password', function () {
        this.login(this.user)
        cy.visit('/profile.html')
        cy.wait(3000)

        let newpassword = 'newpassord'

        //System - should not change password if password1 != password
        cy.get('#btn-edit-password').click();
        cy.get('input[name="current"]').type(this.user.password) //type wrong current password
        cy.get('input[name="password"]').type(newpassword) //Type new passord
        cy.get('input[name="password1"]').type('wrongpassword') //Confirm new password

        cy.get('#btn-save-password').click() //click to save
        cy.contains('Could not change password').parent().should('have.class', 'alert-warning') //should display alert warning
        cy.contains('Could not change password').parent().should('contain', 'Passwords does not match') //warning should contain correct information

        //System - should not change password if current password is not correct
        cy.visit('/profile.html')
        cy.wait(1000)
        cy.get('#btn-edit-password').click();
        cy.get('input[name="current"]').type('wrongpassword') //type wrong current password
        cy.get('input[name="password"]').type(newpassword) //Type new passord
        cy.get('input[name="password1"]').type(newpassword) //Confirm new password

        cy.get('#btn-save-password').click() //click to save
        cy.contains('Could not change password').parent().should('have.class', 'alert-warning') //should display alert warning
        cy.contains('Could not change password').parent().should('contain', 'Current password not correct') //warning should contain correct information

        //System - should successfully change password
        cy.get('#btn-edit-password').click();
        cy.get('input[name="current"]').type(this.user.password) //current password
        cy.get('input[name="password"]').type(newpassword) //Type new passord
        cy.get('input[name="password1"]').type(newpassword) //Confirm new password

        cy.get('#btn-save-password').click() //click to save
        cy.contains('Password sucessfully changed').parent().should('have.class', 'alert-success') //should display alert warning

        this.user.password = newpassword
    })
})

describe('Workout page', function () {
    /*before(function () {
        //add workouts before testing
        this.login(this.user)
        for (var i = 1; i < 5; i++) {
            addWorkout('workout'+i, '2021-02-15T15:33', 'Public', 'notes', "Push-up", i, i)
        }
    })  */

    it('pagination', function () {
        //Visit workout page
        /*cy.visit('/workouts.html')
        cy.url().should('include', 'workouts.html')
        cy.wait(3000)

        //Integration - List should only be one page
        cy.get('#list-my-workouts-list').click()
        cy.get('#slideContainer ul').children().should('have.length', '1')

        //Add one more workout
        addWorkout('workout6', '2021-02-15T15:33', 'Public', 'notes', "Push-up", 10, 10)
        cy.wait(3000)

        //Integration - List should have two pages
        cy.get('#list-my-workouts-list').click()
        cy.get('#slideContainer ul').children().should('have.length', '2')

        //System - page 1 should have 4 workouts
        cy.get('#div-content').children().should('have.length', 4)
        
        //System - page 2 should include 1 workout. 
        cy.get('#pagination2').click()
        cy.get('#div-content').children().should('have.length', 1)*/
    })

    it('previous performance', function () {
        /*this.login(this.user)
        cy.visit('/workout.html')
        cy.url().should('include', 'workout.html')

        //Integration - No previous performance on crunches
        cy.get('select[name="type"]').eq(0).select('Crunch', { force: true })
        cy.get('.performance-button').should('not.be.visible')

        //Integration - Previous performance on Push-up
        cy.get('select[name="type"]').eq(0).select('Push-up', { force: true })
        cy.get('.performance-button').should('be.visible').click({force: true})

        //System - Check if previous performance is correct
        cy.get('#performance-table').children().should('have.length', 5)*/
    })

    function addWorkout(name, date, visibility, notes, type, sets, number) {
        cy.visit('/workout.html')
        cy.get('input[name="name"]').type(name)
        cy.get('input[name="date"]').type(date)
        cy.get('#inputVisibility').select(visibility)
        cy.get('textarea[name="notes"]').type(notes)


        //Add exercise
        cy.get('select[name="type"]').eq(0).select(type, { force: true })
        cy.get('input[name="sets"]').eq(0).type(sets, { force: true })
        cy.get('input[name="number"]').eq(0).type(number, { force: true })

        cy.get('#btn-ok-workout').click()
    }

})