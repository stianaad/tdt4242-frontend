// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')
/// <reference types="Cypress" />

let apiUrl = 'localhost:8080'

let user = {
  username: 'testuser' + Math.floor(Math.random() * 111111),
  password: 'test',
  email: 'test@test.com',
  phone_number: 12345678,
  street_address: 'Street 1',
  city: 'City',
  country: 'Country'
}

function login(user) {
  cy.visit("/login.html")
  cy.url().should('include', '/login.html')

  cy.get('input[name="username"]').type(user.username)
  cy.get('input[name="password"]').type(user.password)

  cy.get('#btn-login').click()
  cy.wait(3000)
}

function register(user) {
  cy.visit("/register.html")
  cy.url().should('include', '/register.html')
  cy.get('input[name="username"]').type(user.username)
  cy.get('input[name="password"]').type(user.password)
  cy.get('input[name="password1"]').type(user.password)

  cy.get('#btn-create-account').click()
  cy.wait(4000)

  logout()
}

function logout() {
  cy.get('#btn-logout', { timeout: 10000 }).click()
  cy.url().should('include', 'index.html')
}


before(() => {
  register(user)
  cy.wrap(user).as('user')
  cy.wrap(login).as('login')
})