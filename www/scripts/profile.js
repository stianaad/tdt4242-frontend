let editButton;
let saveButton;
let cancelButton; 
let oldFormData;
let username; 

let passwordModal;
let passwordButton;
let passwordSaveButton;
let passwordCancelButton; 

async function fetchUser(){
    
    let response = await sendRequest("GET", `${HOST}/api/users?user=current`);

    if (!response.ok) {
        let data = await response.json().response[0];
        let alert = createAlert("Could not retrieve exercise data!", data);
        document.body.prepend(alert);
    }else{
        let userData = await response.json();
        let form = document.querySelector("#form-profile");
        let formData = new FormData(form)

        for (let key of formData.keys()){
            let selector = `input[name="${key}"], textarea[name="${key}"]`;
            let input = form.querySelector(selector);
            let newVal = userData.results[0][key];
            input.value = newVal;
        }

        username = userData.results[0]["username"]
    }

    passwordModal = new bootstrap.Modal(document.getElementById('password-modal'));
}

function closeEditView() {
    setReadOnly(true, "#form-profile");
    saveButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");
    passwordButton.className = passwordButton.className.replace(" hide", "");
}

function handleEditProfileButtonClick() {
    setReadOnly(false, "#form-profile");
    editButton.className += " hide";
    passwordButton.className += " hide";
    saveButton.className = saveButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelDuringEdit);

    let form = document.querySelector("#form-profile");
    oldFormData = new FormData(form);
}

function handleCancelDuringEdit() {
    closeEditView();

    cancelButton.removeEventListener("click", handleCancelDuringEdit);

    let form = document.querySelector("#form-profile");
    if(oldFormData.has("username")) form.username.value = oldFormData.get("username");
    if(oldFormData.has("email")) form.email.value = oldFormData.get("email");
    if(oldFormData.has("phone_number")) form.phone_number.value = oldFormData.get("phone_number");
    if(oldFormData.has("country")) form.country.value = oldFormData.get("country");
    if(oldFormData.has("city")) form.city.value = oldFormData.get("city");
}

async function updateUserInfo(){
    let currentUser = await getCurrentUser();
    let form = document.querySelector("#form-profile");
    let formData = new FormData(form);
    let body = {
        username:       formData.get("username"),
        email:          formData.get("email"),
        phone_number:   formData.get("phone_number"),
        country:        formData.get("country"),
        city:           formData.get("city"),
        street_address: formData.get("street_address")
    }
    console.log("body ejnkwdbhjwnwkn", body);
    let response = await sendRequest("PATCH", currentUser.url, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert('Could not update user profile', data);
        document.body.prepend(alert);
    
    } else {
        //hide buttons from edit view
        closeEditView()
        oldFormData.delete("username");
        oldFormData.delete("phone_number");
        oldFormData.delete("country");
        oldFormData.delete("city");
        
        let success = createSuccessAlert("User information sucessfully updated")
        document.body.prepend(success)
    }
}

async function openPasswordModal(){
    passwordCancelButton = document.querySelector("#btn-cancel-password");
    passwordCancelButton.addEventListener("click", closePasswordModal);
    
    passwordSaveButton = document.querySelector("#btn-save-password");
    passwordSaveButton.addEventListener("click", savePassword)

    passwordModal.show();
}

function closePasswordModal(){
    document.getElementById("form-profile-password").reset();
    passwordModal.hide();
}

async function savePassword(){
    let form = document.querySelector("#form-profile-password");
    let formData = new FormData(form);

    let currentUser = await getCurrentUser();
    console.log(currentUser)

    let body = {"username": username, "current_password": formData.get("current"), "password": formData.get("password"), "password1": formData.get("password1")};

    let response = await sendRequest("PUT",`${HOST}/api/changepassword/${currentUser.id}/`, body)


    if(response.ok){
        let success = createSuccessAlert("Password sucessfully changed")
        document.body.prepend(success)
    }

    else{
        let data = await response.json();
        let alert = createAlert('Could not change password', data);
        document.body.prepend(alert)
    }

    closePasswordModal();
}

window.addEventListener("DOMContentLoaded", async () => {
    editButton = document.querySelector("#btn-edit-profile");
    saveButton = document.querySelector("#btn-save-edit-profile");
    cancelButton = document.querySelector("#btn-cancel-edit-profile");
    passwordButton = document.querySelector("#btn-edit-password");

    await fetchUser()

    editButton.addEventListener("click", handleEditProfileButtonClick)
    saveButton.addEventListener("click", (async () => await updateUserInfo()))
    passwordButton.addEventListener("click", (async () => await openPasswordModal()))
});