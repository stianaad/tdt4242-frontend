async function fetchWorkouts(workouts) {
    let container = document.getElementById('div-content');
    container.innerHTML= '';
    workouts.forEach(workout => {
        let templateWorkout = document.querySelector("#template-workout");
        let cloneWorkout = templateWorkout.content.cloneNode(true);

        let aWorkout = cloneWorkout.querySelector("a");
        aWorkout.href = `workout.html?id=${workout.id}`;

        let h5 = aWorkout.querySelector("h5");
        h5.textContent = workout.name;

        let localDate = new Date(workout.date);

        let table = aWorkout.querySelector("table");
        let rows = table.querySelectorAll("tr");
        rows[0].querySelectorAll("td")[1].textContent = localDate.toLocaleDateString(); // Date
        rows[1].querySelectorAll("td")[1].textContent = localDate.toLocaleTimeString(); // Time
        rows[2].querySelectorAll("td")[1].textContent = workout.owner_username; //Owner
        rows[3].querySelectorAll("td")[1].textContent = workout.exercise_instances.length; // Exercises

        container.appendChild(aWorkout);
    });
    return workouts;
}

async function fetchAllWorkouts(ordering){
    let response = await sendRequest("GET", `${HOST}/api/workouts/?ordering=${ordering}`);
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        let data = await response.json();
        let workouts = data.results;
        console.log(workouts)
        return workouts
    }
}

async function pagination(numberOfWorkouts, numberOfWorkoutsPerPage){
    var slides = Math.ceil(numberOfWorkouts.length/numberOfWorkoutsPerPage)
    var str = '<nav aria-label="Page navigation example" class="mx-auto"><ul class="pagination" >'

    for(let i=1; i<slides+1;i++) {
    str += '<li class="page-item"><a class="page-link" href="#" id=pagination'+i+'>'+ i + '</a></li>';
    }

    str += '</ul></nav>';
    document.getElementById("slideContainer").innerHTML = str;
}

function createWorkout() {
    window.location.replace("workout.html");
}

window.addEventListener("DOMContentLoaded", async () => {
    const numberOfWorkoutsPerPage = 4;
    let createButton = document.querySelector("#btn-create-workout");
    createButton.addEventListener("click", createWorkout);
    let ordering = "-date";

    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('ordering')) {
        let aSort = null;
        ordering = urlParams.get('ordering');
        if (ordering == "name" || ordering == "owner" || ordering == "date") {
                let aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
                aSort.href = `?ordering=-${ordering}`;
        } 
    } 

    let currentSort = document.querySelector("#current-sort");
    currentSort.innerHTML = (ordering.startsWith("-") ? "Descending" : "Ascending") + " " + ordering.replace("-", "");
    //let currentSortNumber = document.querySelector("#current-sort-number");

    let currentUser = await getCurrentUser();
    // grab username
    if (ordering.includes("owner")) {
        ordering += "__username";
    }
    let allWorkouts = await fetchAllWorkouts(ordering);
    let workouts = allWorkouts

    pagination(workouts, numberOfWorkoutsPerPage)

    fetchWorkouts(workouts.slice(0,numberOfWorkoutsPerPage));

    let elementArray = document.querySelectorAll("a[class=page-link]");
    //BAD SMELLS HERE
    /*elementArray.forEach(e => {
        e.addEventListener("click", () => {
            let pageNumber = parseInt(e.innerHTML); //2
            if((pageNumber)*numberOfWorkoutsPerPage > workouts.length){
                fetchWorkouts(workouts.slice((pageNumber-1)*numberOfWorkoutsPerPage))
            } else {
                fetchWorkouts(workouts.slice((pageNumber-1)*numberOfWorkoutsPerPage, (pageNumber)*numberOfWorkoutsPerPage))
            }
        })
    })*/
    updatePagination(elementArray)
    let tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');
    for (let i = 0; i < tabEls.length; i++) {
        let tabEl = tabEls[i];
        tabEl.addEventListener('show.bs.tab', function (event) {
            //let workoutAnchors = document.querySelectorAll('.workout');
            let copyWorkouts = allWorkouts
            workouts = []
            for(let k = 0; k< allWorkouts.length; k++){
                workouts.push(allWorkouts[k])
            }
            for (let k = copyWorkouts.length-1; k> -1; k--) {
                let workout = copyWorkouts[k];
                //let workoutAnchor = workoutAnchors[j];
                switch (event.currentTarget.id) {
                    case "list-my-workouts-list":
                        if (workout.owner == currentUser.url) {
                            //workoutAnchor.classList.remove('hide');
                        } else {
                            workouts.splice(k,1)
                        }
                        break;
                    case "list-athlete-workouts-list":
                        if (currentUser.athletes && currentUser.athletes.includes(workout.owner)) {
                            //workoutAnchor.classList.remove('hide');
                        } else {
                            workouts.splice(k,1)
                            //workoutAnchor.classList.add('hide');
                        }
                        break;
                    case "list-public-workouts-list":
                        if (workout.visibility == "PU") {
                            //console.log(workoutAnchor)
                            //workoutAnchor.classList.remove('hide');
                        } else {
                            workouts.splice(k, 1)
                        }
                        break;
                    default :
                        workouts = allWorkouts
                        break;
                }
            }
            pagination(workouts, numberOfWorkoutsPerPage)
            fetchWorkouts(workouts.slice(0,numberOfWorkoutsPerPage));
            elementArray = document.querySelectorAll("a[class=page-link]");
            //BAD SMELLS HERE
            /*elementArray.forEach(e => {
                e.addEventListener("click", () => {
                    let pageNumber = parseInt(e.innerHTML); //2
                    if((pageNumber)*numberOfWorkoutsPerPage > workouts.length){
                        fetchWorkouts(workouts.slice((pageNumber-1)*numberOfWorkoutsPerPage))
                    } else {
                        fetchWorkouts(workouts.slice((pageNumber-1)*numberOfWorkoutsPerPage, (pageNumber)*numberOfWorkoutsPerPage))
                    }
                })
            })*/
            updatePagination(elementArray)
        });
    }
    // Function to update the data when a user click on another workouts-page
    function updatePagination(array){
        array.forEach(e => {
            e.addEventListener("click", () => {
                let pageNumber = parseInt(e.innerHTML); //2
                if((pageNumber)*numberOfWorkoutsPerPage > workouts.length){
                    fetchWorkouts(workouts.slice((pageNumber-1)*numberOfWorkoutsPerPage))
                } else {
                    fetchWorkouts(workouts.slice((pageNumber-1)*numberOfWorkoutsPerPage, (pageNumber)*numberOfWorkoutsPerPage))
                }
            })
        })
    }
});